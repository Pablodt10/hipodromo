#include <stdbool.h>

int cargaListaCaballosSys(char nombre[][25], long die[], int victorias[], float ganancias[], char msgError[], char nombreArchivo[]);
bool guardaListaCaballosSys(int i, char nombre[][25], long die[], int victorias[], float ganancias[], char nombreArchivo[]);
bool altaCaballoSys(char nombre[], int die, int victorias, float ganancias, char msg[], char nombreArchivo[]);
bool generaInformeCaballos();
void nuevoListadoCaballos();
void gotoxy(int x, int y);
