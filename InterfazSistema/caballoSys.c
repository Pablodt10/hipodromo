#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <stdbool.h>
#include "../InterfazSistema/caballoSys.h"
#include "../AccesoDatos/CaballoAD.h"
#include "../InterfazUsuario/CaballoIU.h"

//Llama a cargaListaCaballosAd para que, si todo ha ido bien, devuelva el n�mero de caballos que hay en Caballos.txt
int cargaListaCaballosSys(char nombre[][25], long die[], int victorias[], float ganancias[], char msgError[], char nombreArchivo[])
{
    int resultado = cargaListaCaballosAD(nombre, die, victorias, ganancias, nombreArchivo);
    if (resultado == -1)
    {
        sprintf(msgError, "Hay un error porque lo he hecho mal");
        return -1;
    }
    else
    {
        return resultado;
    }
}


//Devuelve los datos que recibe de la funci�n altaCaballoAD, en este caso, los datos de cada caballo que pone el usuario
bool altaCaballoSys(char nombre[], int die, int victorias, float ganancias, char msg[], char nombreArchivo[])
{
    int resultadoAlta = altaCaballoAD(nombre, die, victorias, ganancias, nombreArchivo);
    if (resultadoAlta == -1)
    {
        sprintf(msg, "Hay un error porque lo he hecho mal");
        return -1;
    }
    else
    {
        return resultadoAlta;
    }
}


//Genera la apertura de un archivo en el que aparecen escritos los datos espaciados de cada caballo del archivo caballos.txt
bool generaInformeCaballos()
{
    char nombre[25];
    long die;
    int victorias;
    float ganancias;

    FILE * f;
    FILE * f2;

    f=fopen("BaseDatos\\caballos.txt","rt");
    if (f == NULL)
    {
        printf( "No se puede abrir el archivo caballos.txt.\n");
        exit( 1 );
    }

    f2=fopen("BaseDatos\\infoCaballos.txt","wt");
    if (f2 == NULL)
    {
        printf( "No se puede abrir el archivo caballos.txt.\n");
        exit( 1 );

    }

    fprintf(f2,"---------------------------------------------------\n");
    fprintf(f2,"   Die      Victorias    Ganancias     Nombre   \n");
    fprintf(f2,"---------------------------------------------------\n");

    while (!feof(f)&&(fscanf(f,"%ld",&die)==1))
    {

        fscanf(f, "%d", &victorias);
        fscanf(f, "%f", &ganancias);
        fscanf(f, "%s", nombre);

        escribeCaballoTXT(nombre, die, victorias, ganancias, f2);
    }

    fprintf(f2,"---------------------------------------------------\n");

    fclose(f);
    fclose(f2);

    system("notepad BaseDatos/infoCaballos.txt");

    return true;
}


//Recoge los datos de la funci�n cargaListaCaballosAD, que es el n�mero de caballos del listado.
bool guardaListaCaballosSys(int i, char nombre[][25], long die[], int victorias[], float ganancias[], char nombreArchivo[])
{
    if (guardaListaCaballosAD(i, nombre, die, victorias, ganancias, nombreArchivo) == true)
    {
        return true;
    }
    else
    {
        return false;
    }
}



