#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <conio.h>
#include "InterfazGrafica.h"
#include "CaballoIU.h"
#include "../InterfazSistema/caballoSys.h"
#include <stdbool.h>
#include "InterfazGrafica.h"
#include "InterfazUsuario.h"


//pone en la pantalla las opciones que tiene el usuario para avanzar despu�s de haber entrado en gesti�n de caballos
int menuCaballos()
{
    gotoxy(3,9);
    printf("1-Listado de caballos");
    gotoxy(3,10);
    printf("2-Informe de caballos");
    gotoxy(3,11);
    printf("3-Alta de un caballo");
    gotoxy(3,12);
    printf("4-Actualizar un caballo");
    gotoxy(3,13);
    printf("5-Baja de un caballo");
    gotoxy(3,14);
    printf("6-Caballo con mas victorias");
    gotoxy(3,15);
    printf("7-Caballo con mas ganancias");
    gotoxy(3,16);
    printf("8-Clasificacion de caballos");
    gotoxy(3,17);
    printf("9-Fusion de caballos");
    gotoxy(3,18);
    printf("10-Fusion de listas de caballos");
    gotoxy(3,19);
    printf("0-Menu anterior");
    int resultado = 0;
    resultado = leerOpcionValida("Elige una opcion:", 11);
    return resultado;
}

//imprime los caballos que haya en el archivo que se elija.
void listadoCaballos(char nombreArchivo[500])
{
    pintaCaracterVeces(32,120*33);
    pintaTitulo();
    pintarCuadroMedio(0,4);
    pintarCuadroMedio(61,4);
    pintaCuadroInferior();
    gotoxy(20,5);
    printf("LISTADO DE CABALLOS");

    char nombre[100][25], msg[500];
    long die[100];
    int victorias[100];
    float ganancias[100];
    int i = 0;

    int numCab = cargaListaCaballosSys(nombre, die, victorias, ganancias, msg, nombreArchivo);

    if(numCab == -1)
    {
        system("pause");
        return;
    }

    do
    {
        if(numCab>12)
        {
            if(i<(numCab>>1))
                gotoxy(6, i+9);
            else
                gotoxy(66, i+9-(numCab>>1));
        }
        else
            gotoxy(6,i+9);
        muestraCaballo(nombre[i], die[i], victorias[i], ganancias[i]);
        i++;
    }
    while (i<numCab&&i<24);
    if(numCab>12)
    {
        gotoxy(73,5);
        printf("CONTINUACION DE LISTADO CABALLOS");
        gotoxy(70,7);
        printf("Die     Victorias    Ganancias    Nombre");
    }
    gotoxy(10,7);
    printf("Die     Victorias    Ganancias    Nombre");

    gotoxy(11,26);
    printf("Pulse enter para salir del listado de caballos...");
}

//Muestra por pantalla los datos de los caballos que se desee.
void muestraCaballo(char nombre[], long die, int victorias, float ganancias)
{
    printf("  %6ld   %6d       %9.2f     %s  ", die, victorias, ganancias, nombre);
}

//Lee y almacena los datos que el usuario ponga
void altaCaballoIU()
{
    char nombre[25], msg[500];
    int die;
    int victorias;
    float ganancias;

    pintaCaracterVeces(32,120*33);
    pintaTitulo();
    pintarCuadroMedio(0,4);
    pintarCuadroMedio(61,4);
    pintaCuadroInferior();
    gotoxy(20,5);
    printf("ALTA DE UN CABALLO");

    muestraMensajeInfo("Escriba los datos del caballo... ");

    gotoxy(12,26);
    printf("NOMBRE: ");
    scanf("%s", nombre);
    pintarCuadroMedio(0,4);

    limpiarPantallaMuestraCaballo();
    gotoxy(12,26);
    printf("DIE: ");
    scanf("%d", &die);
    pintarCuadroMedio(0,4);

    limpiarPantallaMuestraCaballo();
    gotoxy(12,26);
    printf("VICTORIAS: ");
    scanf("%d", &victorias);
    pintarCuadroMedio(0,4);

    limpiarPantallaMuestraCaballo();
    gotoxy(12,26);
    printf("GANANCIAS: ");
    scanf("%f", &ganancias);
    pintarCuadroMedio(0,4);

    muestraCaballo2(nombre, die, victorias, ganancias);

    altaCaballoSys(nombre, die, victorias, ganancias, msg, "BaseDatos\\caballos.txt");

    getch();
}

//actualiza los valores de las victorias y/o las ganancias de un determinado caballo
void actualizaCaballoIU()
{
    char nombre[100][25], nombre2[25], msg[500];
    long die[100];
    int victorias[100];
    float ganancias[100];
    int i = 0;

    listadoCaballos("BaseDatos\\caballos.txt");
    gotoxy(72,5);
    printf("       Actualiza un caballo...      ");
    gotoxy(3,5);
    printf("               Actualiza un caballo...                 ");
    muestraMensajeInfo("Ponga el nombre del caballo que desee modificar...");
    limpiaLeerOpcionValida();
    gotoxy(12,26);
    scanf("%s", nombre2);

    int zeta = cargaListaCaballosSys(nombre, die, victorias, ganancias, msg, "BaseDatos\\caballos.txt");

    if(zeta == -1)
    {
        system("pause");
        return;
    }

    int erre = -1;
    for (i=0; i<zeta; i++)
    {
        if (strcmp(nombre[i], nombre2) == 0)
        {
            erre = i;
        }
    }
    if (erre == -1)
    {
        limpiaLeerOpcionValida();
        muestraMensajeInfo("No existe el caballo que desea modificar. Pulse enter para continuar...");
        getch();
    }
    else
    {
        limpiarPantalla();
        inicInterfazUsuario();
        gotoxy(2,10);
        printf("Nombre: %s", nombre2);
        gotoxy(2,12);
        printf("Die: %ld", die[erre]);
        gotoxy(2,14);
        printf("Victorias: %d", victorias[erre]);
        gotoxy(2,16);
        printf("Ganancias: %0.2f", ganancias[erre]);

        muestraMensajeInfo("Introduzca el nuevo numero de victorias...");
        limpiaLeerOpcionValida();
        gotoxy(12,26);
        scanf("%d", &victorias[erre]);
        gotoxy(2,14);
        printf("Nuevas victorias: %d", victorias[erre]);

        muestraMensajeInfo("Introduzca el nuevo numero de ganancias...");
        limpiaLeerOpcionValida();
        gotoxy(12,26);
        scanf("%f", &ganancias[erre]);
        gotoxy(2,16);
        printf("Nuevas ganancias: %0.2f", ganancias[erre]);

        guardaListaCaballosSys(i, nombre, die, victorias, ganancias, "BaseDatos\\caballos.txt");

        limpiaLeerOpcionValida();
        muestraMensajeInfo("Su caballo ha sido actualizado con exito. Pulse enter para continuar...");
        getch();
    }
}

// Elimina el caballo de listado caballos cuyo DIE coincida con el DIE seleccionado por el usuario...
void bajaCaballoIU()
{
    char nombre[100][25];
    long die[100], die2;
    int victorias[100];
    float ganancias[100];
    char msg[100];

    int p = -1;
    int aka = 0;
    int numCaballos = cargaListaCaballosSys(nombre, die, victorias, ganancias, msg, "BaseDatos\\caballos.txt");

    if(numCaballos == -1)
    {
        system("pause");
        return;
    }

    listadoCaballos("BaseDatos\\caballos.txt");
    gotoxy(72,5);
    printf("       Elimina un caballo...      ");
    gotoxy(3,5);
    printf("               Elimina un caballo...                 ");
    muestraMensajeInfo("Introduzca el DIE del caballo que desee dar de baja...");
    limpiaLeerOpcionValida();
    gotoxy(12,26);
    scanf("%ld", &die2);

    for (aka=0; aka<numCaballos; aka++)
    {
        if (die2 == die[aka])
        {
            p = aka;
        }
    }

    if (p == -1)
    {
        limpiaLeerOpcionValida();
        muestraMensajeInfo("No existe el caballo con el DIE seleccionado. Pulse enter para continuar...");
    }

    else
    {
        for(; p<numCaballos; p++)
        {
            strcpy(nombre[p], nombre[p+1]);
            die[p] = die[p+1];
            ganancias[p] = ganancias[p+1];
            victorias[p] = victorias[p+1];
        }
        limpiaLeerOpcionValida();
        muestraMensajeInfo("Su caballo ha sido eliminado con exito. Pulse enter para continuar...");
        numCaballos--;
    }
    getch();
    guardaListaCaballosSys(numCaballos, nombre, die, victorias, ganancias, "BaseDatos\\caballos.txt");
}

//dirige a unas funciones u otras dependiendo del resultado que devuelva men� caballos, que ser� el n�mero que ha elegido el usuario
void gestionMenuCaballos()
{
    int x;

    while (x != 0)
    {
        limpiarPantallaMuestraCaballo();
        gotoxy(0,0);
        pintaCaracterVeces(32,32*120);
        pintaTitulo();
        pintarCuadroMedio(0,4);
        pintarCuadroMedio(61,4);
        pintaCuadroInferior();
        gotoxy(19,5);
        printf("GESTION DE CABALLOS");

        x = menuCaballos();

        switch(x)
        {
        case 1:
            listadoCaballos("BaseDatos\\caballos.txt");
            getch();
            break;

        case 2:
            generaInformeCaballos();
            break;

        case 3:
            altaCaballoIU();
            break;

        case 4:
            actualizaCaballoIU();
            break;

        case 5:
            bajaCaballoIU();
            break;

        case 6:
            caballoMasVictorias();
            break;

        case 7:
            caballoMasGanancias();
            break;

        case 8:
            clasificaCaballos();
            break;

        case 9:
            fusionarCaballos();
            break;

        case 10:
            fusionaListaCaballos();
            break;

        case 0:
            inicInterfazUsuario();
            break;

        default: //�nicamente en el caso del 11 (por la llamada de leer opci�n v�lida)
            limpiarPantallaMuestraCaballo();
            ningunaOpcionEsValida();
        }
    }
}

//muestra por pantalla todos los datos que el usuario ha puesto anteriormente
void muestraCaballo2(char nombre[25], int die, int victorias, float ganancias)
{
    pintaCaracterVeces(32,120*35);
    pintaTitulo();
    pintarCuadroMedio(0,4);
    pintarCuadroMedio(61,4);
    pintaCuadroInferior();
    gotoxy(22,5);
    printf("Nuevo caballo");

    gotoxy(2,10);
    printf("NOMBRE: %s", nombre);

    gotoxy(2,12);
    printf("DIE: %d", die);

    gotoxy(2,14);
    printf("VICTORIAS: %d",victorias);

    gotoxy(2,16);
    printf("GANANCIAS: %0.2f", ganancias);

    limpiarPantallaMuestraCaballo();

    gotoxy(11,26);
    printf("Pulsa cualquier tecla para salir de alta de un caballo...");
}

//limpia todo lo que haya puesto el usuario y "Elige una opci�n", normalmente para imprimir un mensaje
void limpiarPantallaMuestraCaballo()
{
    gotoxy(11,26);
    pintaCaracterVeces(32,100);
}

// muestra un mensaje por pantalla
void ningunaOpcionEsValida()
{
    gotoxy(11,29);
    printf("Por favor, seleccione un numero que se encuentre entre las opciones");
    gotoxy (12,26);
    printf("Pulse enter para continuar...");
    getch();
}

//Imprime el nombre del caballo de la lista de caballos que cuenta con mayor cantidad de victorias...
void caballoMasVictorias()
{
    char nombre[100][25], msg[500];
    long die[100];
    int victorias[100];
    float ganancias[100];

    int maxVic = -1;
    int i = 0;
    int s = 0;
    int nCab = cargaListaCaballosSys(nombre, die, victorias, ganancias, msg, "BaseDatos\\caballos.txt");

    if(nCab == -1)
    {
        system("pause");
        return;
    }

    listadoCaballos("BaseDatos\\caballos.txt");

    for (i=0; i<nCab; i++)
    {
        if (maxVic<victorias[i])
        {
            maxVic = victorias[i];
            s = i;
        }
    }

    limpiaLeerOpcionValida();
    gotoxy(11,26);
    printf("El caballo que mas victorias tiene actualmente es %s. Pulse enter para continuar...", nombre[s]);
    getch();
}

//Imprime el nombre del caballo de la lista de caballos que cuenta con mayor cantidad de ganancias...
void caballoMasGanancias()
{
    char nombre[100][25], msg[500];
    long die[100];
    int victorias[100];
    float ganancias[100];

    int numC = cargaListaCaballosSys(nombre, die, victorias, ganancias, msg, "BaseDatos\\caballos.txt");
    int maxGan = -1;
    int i = 0;
    int q = 0;

    listadoCaballos("BaseDatos\\caballos.txt");

    for (i=0; i<numC; i++)
    {
        if (maxGan<ganancias[i])
        {
            maxGan = ganancias[i];
            q = i;
        }
    }
    limpiaLeerOpcionValida();
    gotoxy(11,26);
    printf("El caballo que mas dinero acumulado tiene actualmente es %s. Pulse enter para continuar...", nombre[q]);
    getch();
}

//Funci�n que muestra por pantalla �nicamente los caballos que superen o igualen las victorias que ponga el usuario.
void clasificaCaballos()
{
    pintaCaracterVeces(32,120*35);
    pintaTitulo();
    pintarCuadroMedio(0,4);
    pintarCuadroMedio(61,4);
    pintaCuadroInferior();

    char nombre1[100][25];
    long die1[100];
    int victorias1[100];
    float ganancias1[100];

    char nombre2[100][25];
    long die2[100];
    int victorias2[100];
    float ganancias2[100];

    char nombreArchivo[500], msg[500];
    int k,a = 0;
    int vic = 0;

    gotoxy(11,26);
    printf("Ponga el numero de victorias que tienen que superar los caballos: ");
    scanf("%d", &vic);
    limpiaLeerOpcionValida();
    gotoxy(11,26);
    printf("Introduzca el nombre del archivo en el que quiera que se guarden los resultados: ");
    scanf("%s", nombreArchivo);

    int numero1 = cargaListaCaballosSys(nombre1, die1, victorias1, ganancias1, msg, "BaseDatos\\caballos.txt");

    if(numero1 == -1)
    {
        system("pause");
        return;
    }

    for (k=0; k<numero1; k++)
    {
        if (victorias1[k]>=vic)
        {
            strcpy(nombre2[a], nombre1[k]);
            die2[a] = die1[k];
            victorias2[a] = victorias1[k];
            ganancias2[a] = ganancias1[k];
            a++;
        }
    }
    guardaListaCaballosSys(a, nombre2, die2, victorias2, ganancias2, nombreArchivo);
    listadoCaballos(nombreArchivo);
    gotoxy(6,5);
    printf("CABALLOS QUE HAN ALCANZADO LA PRIMERA DIVISION");
    gotoxy(11,26);
    printf("Estos son los caballos que han superado sus pruebas de admision. Pulse enter para continuar...");
    getch();
}

//Funci�n que fusiona los caballos de los archivos caballos1.txt y caballos2.txt
void fusionarCaballos()
{
    char msg[500];

    char nombre1[100][25];
    long die1[100];
    int victorias1[100];
    float ganancias1[100];

    char nombre2[100][25];
    long die2[100];
    int victorias2[100];
    float ganancias2[100];

    listadoCaballos("BaseDatos\\caballos1.txt");
    gotoxy(11,26);
    printf("Estos son los caballos de la primera lista. Pulse enter para continuar...");
    getch();
    listadoCaballos("BaseDatos\\caballos2.txt");
    gotoxy(11,26);
    printf("Estos son los caballos de la segunda lista. Pulse enter para continuar...");
    getch();

    int numero1 = cargaListaCaballosSys(nombre1, die1, victorias1, ganancias1, msg, "BaseDatos\\caballos1.txt");
    int numero2 = cargaListaCaballosSys(nombre2, die2, victorias2, ganancias2, msg, "BaseDatos\\caballos2.txt");

    if(numero1 == -1)
    {
        system("pause");
        return;
    }

    if(numero2 == -1)
    {
        system("pause");
        return;
    }

    int i,j = 0;
    for(i=0; i<numero2; i++)
    {
        for(j=0; j<numero1; j++)
        {
            if (strcmp(nombre2[i], nombre1[j]) == 0)
            {
                victorias1[j] = victorias1[j] + victorias2[i];
                ganancias1[j] = ganancias1[j] + ganancias2[i];
                break;
            }
        }
        if (strcmp(nombre2[i], nombre1[j])!=0)
        {
            strcpy(nombre1[numero1], nombre2[i]);
            die1[numero1] = die2[i];
            victorias1[numero1] = victorias2[i];
            ganancias1[numero1] = ganancias2[i];
            numero1++;
        }
    }
    guardaListaCaballosSys(numero1, nombre1, die1, victorias1, ganancias1, "BaseDatos\\caballos3.txt");
    muestraListadoCaballos(nombre1, die1, victorias1, ganancias1);
    gotoxy(11,26);
    printf("Estos son los caballos fusionados con sus victorias y ganancias. Pulse enter para continuar...");
    getch();
}

//Funci�n que muestra por pantalla los caballos que se han guardado en el archivo txt3.
void muestraListadoCaballos(char nombre1[][25], long die1[], int victorias1[], float ganancias1[])
{
    pintaCaracterVeces(32,120*35);
    pintaTitulo();
    pintarCuadroMedio(0,4);
    pintarCuadroMedio(61,4);
    pintaCuadroInferior();

    char msg[500];
    int a = 9;
    int g = 9;
    int i = 0;

    gotoxy(10,7);
    printf("Die     Victorias    Ganancias    Nombre");
    gotoxy(20,5);
    printf("FUSION DE CABALLOS");

    int numero3 = cargaListaCaballosSys(nombre1, die1, victorias1, ganancias1, msg, "BaseDatos\\caballos3.txt");

    if(numero3 == -1)
    {
        system("pause");
        return;
    }

    if (numero3<8)
    {
        for(i=0; i<numero3; i++)
        {
            gotoxy(5,a);
            printf("\t%6ld\t    %3d\t       %0.2f\t    %s\t", die1[i], victorias1[i], ganancias1[i], nombre1[i]);
            a++;
        }
    }
    else
    {
        for(i=0; i<8; i++)
        {
            gotoxy(5,a);
            printf("\t%6ld\t    %3d\t       %0.2f\t    %s\t", die1[i], victorias1[i], ganancias1[i], nombre1[i]);
            a++;
        }
        for (i=8; i<numero3; i++)
        {
            gotoxy(80,5);
            printf("FUSION DE CABALLOS");
            gotoxy(70,7);
            printf("Die     Victorias    Ganancias    Nombre");
            gotoxy(65,g);
            printf("   %6ld\t %3d\t   %0.2f\t%s\t", die1[i], victorias1[i], ganancias1[i], nombre1[i]);
            g++;
        }
    }
}

//Funci�n que fusiona las listas de caballos dependiendo de si coincide el die de los caballos.
void fusionaListaCaballos()
{
    char msg[500];

    char nombre1[100][25];
    long die1[100];
    int victorias1[100];
    float ganancias1[100];

    char nombre2[100][25];
    long die2[100];
    int victorias2[100];
    float ganancias2[100];

    char nombre3[100][25];
    long die3[100];
    int victorias3[100];
    float ganancias3[100];

    listadoCaballos("BaseDatos\\caballos1.txt");
    gotoxy(11,26);
    printf("Estos son los caballos de la primera lista. Pulse enter para continuar...");
    getch();
    listadoCaballos("BaseDatos\\caballos2.txt");
    gotoxy(11,26);
    printf("Estos son los caballos de la segunda lista. Pulse enter para continuar...");
    getch();

    int u = cargaListaCaballosSys(nombre1, die1, victorias1, ganancias1, msg, "BaseDatos\\caballos1.txt");
    int e = cargaListaCaballosSys(nombre2, die2, victorias2, ganancias2, msg, "BaseDatos\\caballos2.txt");

    if(u == -1)
    {
        system("pause");
        return;
    }
    if(e == -1)
    {
        system("pause");
        return;
    }

    int i,j,a = 0;

    for (i=0; i<e; i++)
    {
        for (j=0; j<u; j++)
        {
            if (die2[i] == die1[j])
            {
                strcpy(nombre3[a], nombre1[j]);
                die3[a] = die1[i];
                victorias3[a] = victorias1[j] + victorias2[i];
                ganancias3[a] = ganancias1[j] + ganancias2[i];
                a++;
            }
        }
    }
    guardaListaCaballosSys(a, nombre3, die3, victorias3, ganancias3, "BaseDatos\\caballos3.txt");
    muestraListadoCaballos(nombre3, die3, victorias3, ganancias3);
    gotoxy(11,26);
    printf("Estos son los caballos de las listas fusionados. Pulse enter para continuar...");
    getch();
}
