void setColorTexto(WORD colors);
void muestraMensajeInfo(char []);
void gotoxy(int x,int y);

void pintarLinea(int n);
void pintarLineaDoble(int n);
void pintarColumna(int n,int x,int y);

int leerOpcionValida(char *mensaje, char num);

void inicInterfazUsuario();
int menuPrincipal();
void gestionMenuPrincipal(void);
void pintaCaracterVeces();
void pintaTitulo();
void gestionMenuCaballos();
void limpiarPantalla();
void limpiarNumero();
void limpiaLeerOpcionValida();
