#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <conio.h>
#include "InterfazGrafica.h"
#include "InterfazUsuario.h"
#include "../InterfazUsuario/CaballoIU.h"

//Definici�n de la funci�n de gotoxy.
void gotoxy(int x, int y)
{
    COORD coord;

    coord.X = x;
    coord.Y = y;

    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);

    return;
}

//Muestra un mensaje opcional en un sitio fijo.
void muestraMensajeInfo(char msg[])
{
    gotoxy(12,24);
    printf("%s", msg);
}

//Recoge un valor puesto por el usuario y lo devuelve para que la funci�n que la llame recoja dicho valor.
int leerOpcionValida(char *mensaje, char num)
{
    int resultado;
    gotoxy(12,26);
    printf("%s ", mensaje);
    int scaResult = scanf("%d", &resultado);
    while(scaResult!=1 || resultado <0 || resultado > num)
    {
        limpiarNumero();
        gotoxy(12,26);
        printf("La opcion no se encuentra en el menu. Pulse enter para continuar...");
        getch();
        limpiaLeerOpcionValida();
        gotoxy(12,26);
        printf("%s ", mensaje);
        scanf("%d", &resultado);
    }
    return resultado;
}

//Definici�n de la funci�n que cambia el color de la consola de comandos.
void SetColorTexto(WORD colors)
{
    HANDLE hConsole=GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hConsole, colors);
}

//Limpia s�lo la parte de abajo del cuadro, en la que es muy frecuente escribir mensajes.
void limpiaLeerOpcionValida()
{
    gotoxy(11,26);
    pintaCaracterVeces(32,98);
}

