#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "InterfazGrafica.h"
#include "InterfazUsuario.h"
#include "../InterfazUsuario/InterfazGrafica.h"
#include "../InterfazUsuario/caballoIU.h"

//Define las dimensiones de la consola que se abre al ejecutar el programa.
//Pone como t�tulo a la consola "Hipodromo".
//Aplica el color seleccionado a toda la pantalla.
//Llama a todas las funciones que se encargan de pintar el cuadro.
void inicInterfazUsuario()
{
    system("mode con cols=120 lines=32");
    system("title Hipodromo");
    system("color 03");
    pintaTitulo();
    pintarCuadroMedio(0,4);
    pintarCuadroMedio(61,4);
    pintaCuadroInferior();
}

//Dependiendo del valor que recoja de la funci�n menuPrincipal, dirige a unas funciones u otras seg�n los casos del switch.
void gestionMenuPrincipal()
{
    int valor;
    valor = menuPrincipal();

    while (valor!=0)
    {
        switch(valor)
        {

        case 1:
            gestionMenuCaballos();
            break;

        case 2:
        case 3:

            limpiarPantalla();
            limpiarNumero();
            gotoxy(6,29);
            printf("Opcion no implementada.");
            break;

        default:
            limpiarNumero();
            gotoxy(6,29);
            printf("Por favor, seleccione una opcion del menu.");
            getchar();
            break;

        }
        valor = menuPrincipal();
    }
}

//Imprime todas las opciones que el usuario tiene para seleccionar.
//Escanea un valor que le mete el usuario y lo manda directamente a lo devuelve.
int menuPrincipal()
{
    gotoxy(22,5);
    printf("MENU PRINCIPAL");
    gotoxy(4,10);
    printf("1.Gestion de caballos");
    gotoxy(4,12);
    printf("2.Gestion de carreras");
    gotoxy(4,14);
    printf("3.Inicio de carrera");
    gotoxy(4,16);
    printf("0.Fin del programa");
    gotoxy(12,26);
    printf("Seleccione una opcion: ");
    int c;
    scanf("%d",&c);
    return c;
}

//Pinta el cuadro superior, junto al t�tulo de HIPODROMO.
void pintaTitulo ()
{
    gotoxy(0,0);
    pintaCaracterVeces(201,1);
    pintaCaracterVeces(205,118);
    pintaCaracterVeces(187,1);
    gotoxy(0,1);
    pintaCaracterVeces(186,1);
    gotoxy(119,1);
    pintaCaracterVeces(186,1);
    gotoxy(0,2);
    pintaCaracterVeces(200,1);
    pintaCaracterVeces(205,118);
    pintaCaracterVeces(188,1);

    gotoxy(55,1);
    printf("HIPODROMO");
}

//Pinta todo el cuadro medio (subdividido en dos cuadros sim�tricos).
void pintarCuadroMedio (int x, int y)
{
    //linea recta:
    gotoxy(x,y);
    pintaCaracterVeces(218,1);
    //esquinas:
    pintaCaracterVeces(196,57);
    pintaCaracterVeces(191,1);
    //laterales:
    pintaLateralesCuadroMedio(x,y,179,179);
    pintaLateralesCuadroMedio(x,y+1,195,180);
    pintaLateralesCuadroMedio(x,y+2,179,179);
    pintaLateralesCuadroMedio(x,y+3,195,180);

    int i;
    for (i=4; i<16; i++)
    {
        pintaLateralesCuadroMedio(x,y+i,179,179);
    }

    pintaLateralesCuadroMedio(x,y+16,192,217);
    gotoxy(x+1,y+2);
    pintaCaracterVeces(196,57);
    gotoxy(x+1,y+4);
    pintaCaracterVeces(196,57);
    gotoxy(x+1,y+17);
    pintaCaracterVeces(196,57);
}

//Pinta los laterales del cuadro medio.
void pintaLateralesCuadroMedio (int x,int y, int caracterIzquierdo, int caracterDerecho)
{
    gotoxy(x,y+1);
    pintaCaracterVeces(caracterIzquierdo,1);
    gotoxy(x+58,y+1);
    pintaCaracterVeces(caracterDerecho,1);
}

//Funcion que se encarga de pintar el caracter que se desee las veces que se desee.
void pintaCaracterVeces(int caracter, int veces)
{
    int i;
    for (i=0; i<veces; i++)
    {
        printf("%c", caracter);
    }
}

//Pinta todo el cuadro inferior.
void pintaCuadroInferior ()
{
    //linea superior:
    gotoxy(10,23);
    pintaCaracterVeces(218,1);
    pintaCaracterVeces(196,98);
    pintaCaracterVeces(191,1);
    //lineas laterales:
    gotoxy(10,24);
    pintaCaracterVeces(179,1);
    gotoxy(109,24);
    pintaCaracterVeces(179,1);
    //linea intermedia:
    gotoxy(10,25);
    pintaCaracterVeces(195,1);
    pintaCaracterVeces(196,98);
    pintaCaracterVeces(180,1);
    //lineas laterales:
    gotoxy(10,26);
    pintaCaracterVeces(179,1);
    gotoxy(109,26);
    pintaCaracterVeces(179,1);
    //linea inferior:
    gotoxy(10,27);
    pintaCaracterVeces(192,1);
    pintaCaracterVeces(196,98);
    pintaCaracterVeces(217,1);
}

//Limpia una parte de la pantalla que interesa para una funci�n espec�fica.
void limpiarPantalla()
{
    gotoxy(5,29);
    pintaCaracterVeces(32,50);
}

//Limpia los n�meros que se pongan cuando las funciones utilizan el scanf en ese gotoxy (muy frecuente).
void limpiarNumero()
{
    gotoxy(25,26);
    pintaCaracterVeces(32,50);
}





















































