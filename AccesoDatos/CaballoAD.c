#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <stdbool.h>
#include "../AccesoDatos/CaballoAD.h"
#include "../InterfazSistema/caballoSys.h"

//imprime todos los datos de cada caballo del archivo caballos.txt en orden.
int altaCaballoAD(char nombre[], long die, int victorias, float ganancias, char nombreArchivo[])
{
    FILE *ptr;
    ptr = fopen(nombreArchivo, "a+");

    if (ptr == NULL)
    {
        printf("\n\n\n\nError de apertura del archivo");
        return -1;
    }
    fprintf(ptr, "%ld %d %0.2f %s\n", die, victorias, ganancias, nombre);

    fclose(ptr);
    return 0;
}


void escribeCaballoTXT(char nombre[], long die, int victorias, float ganancias, FILE *ptr)
{
    fprintf(ptr, "  %ld\t", die);
    fprintf(ptr, "%d\t", victorias);
    fprintf(ptr, "%8.2f\t", ganancias);
    fprintf(ptr, "%s\n", nombre);
}

//devuelve el n�mero de ejecuciones del bucle while, es decir, el n�mero de caballos que se encuentran en caballos.txt
int cargaListaCaballosAD(char nombre[][25], long die[], int victorias[], float ganancias[], char nombreArchivo[25])
{
    int cuantosCaballosLlevo = 0;

    FILE *ptr;
    ptr = fopen(nombreArchivo, "rt");

    if (ptr == NULL)
    {
        printf("\n\n\n\nError de apertura del archivo");
        return -1;
    }
    while (!feof(ptr))
    {
        fscanf(ptr, "%ld", &die[cuantosCaballosLlevo]);
        if (feof(ptr))
        {
            break;
        }
        fscanf(ptr, "%d", &victorias[cuantosCaballosLlevo]);
        fscanf(ptr, "%f", &ganancias[cuantosCaballosLlevo]);
        fscanf(ptr, "%s", nombre[cuantosCaballosLlevo]);
        cuantosCaballosLlevo++;
    }

    fclose(ptr);

    return cuantosCaballosLlevo;
}

//Guarda los datos nuevos de nuevos caballos en el archivo caballos.txt

bool guardaListaCaballosAD(int i, char nombre[][25], long die[], int victorias[], float ganancias[], char nombreArchivo[500])
{
    int a;
    FILE *ptr;
    ptr = fopen(nombreArchivo, "wt");

    if (ptr == NULL)
    {
        printf("\n\n\n\nError de apertura del archivo");
        return false;
    }
    for (a=0; a<i; a++)
        escribeCaballoTXT(nombre[a], die[a], victorias[a], ganancias[a], ptr);
    fclose(ptr);
    return true;
}



