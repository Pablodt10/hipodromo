#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "InterfazUsuario\InterfazGrafica.h"
#include "InterfazUsuario\InterfazUsuario.h"

//Funci�n principal que llama a las dos funciones puestas, y finalmente el programa finaliza devolviendo el valor 0 (por convenio)
int main()
{
    inicInterfazUsuario();
    gestionMenuPrincipal();
    return 0;
}

